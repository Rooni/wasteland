import {h, app} from "https://unpkg.com/hyperapp"
import {} from "https://unpkg.com/lodash"

const socket = new WebSocket("ws://127.0.0.1:6789/");

socket.addEventListener('open', function (event) {
    socket.send(JSON.stringify({action: 0, id: "web"}));
});

socket.onmessage = function (event) {
    const data = JSON.parse(event.data);
    const type = data.type;
    const state = data.state;

    if (type === 100) {
        const event = new Event("state");
        event.data = state;

        document.getElementById("log").textContent = JSON.stringify(state);
        document.getElementById("app").dispatchEvent(event);
    }
};

app({
    init: {count: 0},
    view: state => {
        console.log('state', state);
        return h("main", {onstate: (state, e) => e.data}, [
            Title(state),
            h('h2', {}, state.message),
            Content(state)
        ])
    },
    node: document.getElementById("app")
});

function Title(state) {
    return h('h1', [], getTitle(state))
}

function getTitle(state) {
    switch (state.state) {
        case "RAID":
            return "Raid";
        case "COLLECT_PICK":
            return "Collect";
        case "COLLECT_PICK_TURN":
            return "Collect";
        default:
            return "Wasteland"
    }
}

function Content(state) {
    switch (state.state) {
        case "COLLECT_PICK":
        case "COLLECT_PICK_TURNS":
            return Collect(state);
        default:
            return SelectAction();
    }
}

function SelectAction() {
    return Buttons('Raid', 'Collect', 'Card')
}

function Collect(state) {
    return Buttons('-', 'OK', '+')
}

function Buttons(first, second, third) {
    return h('div', {}, [
        h('div', {class: 'buttons'}, [
                Button(first, 'raid button', 1),
                Button(second, 'collect button', 2),
                Button(third, 'card button', 3)
            ]
        )
    ]);
}

function Button(name, className, action) {
    return h(
        'div',
        {
            class: className,
            onClick: () => {
                socket.send(JSON.stringify({action: action}))
            }
        },
        name
    )
}