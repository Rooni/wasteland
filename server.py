#!/usr/bin/env python
import asyncio
import random
import json
import logging
from enum import Enum, IntEnum

import websockets


logging.basicConfig(level=logging.INFO)

cards = [

]


class Action(IntEnum):
    JOIN = 0
    WELCOME = 6
    # Main actions
    RAID = 1
    COLLECT = 2
    CARD = 3
    # Bop
    RAISE = 8
    LOWER = 9

    # State update
    STATE = 100


class State(IntEnum):
    WAITING = 0
    PLAYER_TURN = 1
    RAID_PICK = 2
    COLLECT_PICK = 3
    COLLECT_PICK_TURNS = 4

class PlayerState:
    __slots__ = ['id', 'color', 'gold', 'resources', 'units', 'wasteland_units', 'choice']

    def __init__(self, id, color):
        self.id = id
        self.color = color
        self.gold = 3
        self.units = 3
        self.wasteland_units = 0

        self.choice = 0

    def __dict__(self):
        return {
            'id': self.id,
            'color': self.color,
            'gold': self.gold,
            'units': self.units,
            'wasteland_units': self.wasteland_units,
            'choice': self.choice
        }

    def __str__(self):
        return f"Player {self.id} ({self.color})"

class GameState:
    __slots__ = ['current_player', 'players', 'turn', 'state', 'message']

    def __init__(self):
        self.current_player = 0
        self.players = [PlayerState(i, c) for i, c in enumerate(['yellow', 'red', 'white', 'green'])]
        self.turn = 1
        self.state = State.PLAYER_TURN
        self.message = ""

    def data(self):
        return {
            'current_player': self.current_player,
            'state': self.state.name,
            'player_data': [player_state.__dict__() for player_state in self.players],
            'message': self.message,
            'player': self.players[self.current_player].__dict__()
        }


class Game:
    def __init__(self):
        self.state = GameState()
        self.sockets = dict()

    def register(self, id, socket):
        self.sockets[id] = socket

    def unregister(self, socket):
        logging.info(f"Bye")
        self.sockets = {k: v for k, v in self.sockets.items() if v != socket}

    async def publish_state(self):
        if self.sockets:
            await asyncio.wait([sock.send(json.dumps({"type": Action.STATE, "state": self.state.data()})) for sock in self.sockets.values()])

    async def loop(self, socket, path):
        try:
            await socket.send(json.dumps({'type': Action.WELCOME}))
            async for message in socket:
                data = json.loads(message)
                logging.info(data)
                action = data['action']
                state = self.state.state
                if state == State.PLAYER_TURN:
                    # Current player selects one of the three base options
                    if action == Action.RAID:
                        self.state.message = f"Who do you want to raid? "
                        available = [p for i, p in enumerate(self.state.players) if i != self.state.current_player]
                        self.state.message += " ".join([f"{i}: {p.color}" for i, p in enumerate(available)])
                        self.state.state = State.RAID_PICK
                    elif action == Action.COLLECT:
                        self.state.message = "Use - and + to set units"
                        self.state.state = State.COLLECT_PICK
                        self.player.choice = 1
                    elif action == Action.CARD:
                        pass
                    else:
                        logging.error(f"Unknown action {action} during state {self.state}")
                elif state == State.RAID_PICK:
                    # Current player picks an opponent to raid.
                    available = [p for i, p in enumerate(self.state.players) if i != self.state.current_player]
                    picked = action-1
                    self.state.message = f"Player {self.state.players[self.state.current_player]} raided {available[picked]}"
                    self.rotate_players()
                    self.state.state = State.PLAYER_TURN
                elif state == State.COLLECT_PICK:
                    if action == Action.RAID:
                        # Decrement unit count
                        self.player.choice = max(self.player.choice-1, 0)
                    elif action == Action.CARD:
                        # Increment unit count
                        self.player.choice = min(self.player.choice+1, self.player.units)
                    elif action == Action.COLLECT:
                        # Confirm unit count
                        self.player.units -= self.player.choice
                        self.player.wasteland_units += self.player.choice
                        self.state.state = State.COLLECT_PICK_TURNS
                elif state == State.COLLECT_PICK_TURNS:
                    if action == Action.RAID:
                        # Decrement turn count
                        self.player.choice = max(self.player.choice-1, 0)
                    elif action == Action.CARD:
                        # Increment turn count
                        self.player.choice = min(self.player.choice+1, self.player.units)
                    elif action == Action.COLLECT:
                        # Confirm turn count
                        self.player.units -= self.player.choice
                        self.player.wasteland_units += self.player.choice
                        self.state.state = State.COLLECT_PICK_TURNS

                if action == Action.JOIN:
                    self.register(data['id'], socket)
                    logging.info(f"Registered client {data['id']}")

                await self.publish_state()
        finally:
            self.unregister(socket)

    def rotate_players(self):
        self.state.current_player = (self.state.current_player + 1) % len(self.state.players)
        if self.state.current_player == 0:
            self.state.turn += 1

    @property
    def player(self):
        return self.state.players[self.state.current_player]

game = Game()

start_server = websockets.serve(game.loop, "localhost", 6789)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()